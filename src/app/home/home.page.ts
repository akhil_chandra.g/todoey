import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  add:boolean = false;
  tasks = [];
  ind;
  show:boolean = false;
  name =  '';
  id = false;
  constructor() {}

  toggle() {
    this.show = !this.show;
  }

  toggleStrike(x) {
    this.ind=x;
    this.tasks[this.ind].id = !this.tasks[this.ind].id;
  }
  nullify() {
    this.name = '';
    this.id = false;
  }
  showAddTask() {
    if(this.add === true && this.name.length > 0){
      console.log(this.name);
      let obj = {name: this.name, id: this.id};
      this.addTask(obj);
      // this.name = '';
      // console.log("welcome");
      this.add = !this.add;
    }
    else if(this.add === true && this.name.length === 0){
      alert("No task entered");
      this.add = !this.add;
    }
    else{
      console.log("bye bye");
      this.add = !this.add;
    }
  }
  addTask(obj) {
    this.tasks.push(obj);
    console.log("tasks " + this.tasks);
  }

}
